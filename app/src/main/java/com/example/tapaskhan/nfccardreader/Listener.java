package com.example.tapaskhan.nfccardreader;

public interface Listener {

    void onDialogDisplayed();
    void onDialogDismissed();

}
