package com.example.tapaskhan.nfccardreader;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements Listener {

    public static final String TAG = MainActivity.class.getSimpleName();

    private NfcAdapter mNfcAdapter;
    private NFCReadFragment mNfcReadFragment;
    private Button mBtRead;
    private boolean isDialogDisplayed = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();
        initNFC();

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }
    private void initViews() {


        mBtRead = (Button) findViewById(R.id.btn_read);
        mBtRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReadFragment();
                //showLaundryOptions("1234");
            }
        });
      // mBtRead.setOnTouchListener(new T2TouchListener());
    }

    private void initNFC(){

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
    }
    private void showReadFragment() {
        Log.d("In read fragment","showing the messgae");

        mNfcReadFragment = (NFCReadFragment) getFragmentManager().findFragmentByTag(NFCReadFragment.TAG);

        if (mNfcReadFragment == null) {

            mNfcReadFragment = NFCReadFragment.newInstance();
        }
        mNfcReadFragment.show(getFragmentManager(),NFCReadFragment.TAG);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogDisplayed() {

       // isDialogDisplayed = true;
    }

    @Override
    public void onDialogDismissed() {

       // isDialogDisplayed = false;

    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter techDetected = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
        IntentFilter[] nfcIntentFilter = new IntentFilter[]{techDetected,tagDetected,ndefDetected};

        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        if(mNfcAdapter!= null)
            mNfcAdapter.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mNfcAdapter!= null)
            mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        //setIntent(intent);

        //super.onNewIntent(intent);
        if (intent != null && (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())
                                || NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction()))) {


            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            Log.d(TAG, "onNewIntent: " + intent.getAction());

            if (tag != null) {

                Toast.makeText(this, getString(R.string.message_tag_detected), Toast.LENGTH_SHORT).show();
                Ndef ndef = Ndef.get(tag);
                mNfcReadFragment.onNfcDetected(ndef);
                String roomId=mNfcReadFragment.getTextMessage();
                Log.d(TAG, "***ROOM ID ****: " + roomId);
                showLaundryOptions(roomId);

                /*LaundryService laundryService = (LaundryService)getSupportFragmentManager().findFragmentByTag(LaundryService.TAG);

                if (laundryService == null) {

                    laundryService = LaundryService.newInstance();
                }
                if(mNfcReadFragment!=null){
                    mNfcReadFragment.dismiss();
                }
                laundryService.show(getSupportFragmentManager(),LaundryService.TAG);*/

                // if (isDialogDisplayed) {

               // mNfcReadFragment = (NFCReadFragment) getFragmentManager().findFragmentByTag(NFCReadFragment.TAG);


                // }
            }
        }
    }

    private void showLaundryOptions(String roomId){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag(LaundryService.TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        if(mNfcReadFragment!=null) {
            mNfcReadFragment.dismiss();
        }

        // Create and show the dialog.
        DialogFragment newFragment = LaundryService.newInstance(roomId);
        newFragment.show(ft, LaundryService.TAG);
    }
}
