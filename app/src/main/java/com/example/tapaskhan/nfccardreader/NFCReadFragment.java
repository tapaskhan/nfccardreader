package com.example.tapaskhan.nfccardreader;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NFCReadFragment extends DialogFragment {

     public static final String TAG = NFCReadFragment.class.getSimpleName();

    public static NFCReadFragment newInstance() {

        return new NFCReadFragment();
    }

    private TextView mTvMessage;
    private Listener mListener;



    private String textMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_read,container,false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {

        mTvMessage = (TextView) view.findViewById(R.id.tv_message);
    }
    public void onNfcDetected(Ndef ndef){

        try {
            readFromNFC(ndef);
        }catch (IOException e) {
            e.printStackTrace();

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void readFromNFC(Ndef ndef) throws IOException{

        try {
            ndef.connect();
            NdefMessage ndefMessage = ndef.getNdefMessage();
            for(NdefRecord record :ndefMessage.getRecords()){
                Log.d(TAG, "recordtype:"+record.getType()+
                        " recordTnf "+record.getTnf()+ " recordPayload "+record.getPayload()
                );

                String message = new String(record.getPayload());
                /*if(isValidPhone(message)){
                    invokePhoneCall(message);
                }
                if(message.contains("sms")){
                    sendSMS(message);
                }
                if(message.contains("http")){
                    openURL(message);
                }*/
                Log.d(TAG, " message: " +message);
                String text =message;
                if(message.contains("en")){
                    Log.d(TAG, " IN EN: " +text);
                    text=message.replaceFirst("en","");
                    Log.d(TAG, " After IN EN : " +text);
                }
                Log.d(TAG, " parsed message: " +text);
                setTextMessage(text);
            }





        } catch (IOException | FormatException e) {
            e.printStackTrace();

        }finally{
            ndef.close();
        }
    }
  /*  @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivity)context;
        mListener.onDialogDisplayed();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.onDialogDismissed();
    }
*/
  public void setTextMessage(String textMessage) {
      this.textMessage = textMessage;
  }

    public String getTextMessage() {
        return textMessage;
    }

     /* private boolean isValidPhone(String s){
        if(s.contains("+")){
            s=s.replaceAll("\\+","");
        }
        Log.d(TAG, " phone: " +s);
        Pattern p = Pattern.compile("^[0-9]*$");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }
    private void invokePhoneCall(String phone){

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+phone));//change the number
        startActivity(callIntent);

    }
    private void sendSMS(String message){//sms:+919830755532?body=Hello from NFC tag
        String phoneNumber=message.substring(message.indexOf("sms:")+1,message.indexOf("?"));
        String messageContent=message.substring(message.indexOf("=")+1,message.length());
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, messageContent, null, null);

    }
    private void openURL(String message){
        String url = message;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }*/

}