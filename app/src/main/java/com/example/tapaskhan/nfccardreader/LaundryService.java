package com.example.tapaskhan.nfccardreader;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LaundryService extends DialogFragment{

    public static final String TAG =LaundryService.class.getSimpleName();
    private TextView mroomId;

    public static LaundryService newInstance(String roomIdText) {
        LaundryService laundryService=new LaundryService();
        Bundle args = new Bundle();
        args.putString("roomIdText", roomIdText);
        laundryService.setArguments(args);
        return laundryService;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view=inflater.inflate(R.layout.laundry_service,null);
        mroomId = (TextView) view.findViewById(R.id.room_id);
        String roomdIdText =getArguments().getString("roomIdText");
        mroomId.setText(roomdIdText);
        builder.setView(view)
                // Add action buttons
               /* .setPositiveButton(R.string.submit_tag, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        System.out.println("***In submit ");
                    }
                })

                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LaundryService.this.getDialog().cancel();
                    }
                })*/
                ;
        return builder.create();
    }


  /*@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.laundry_service,container, false);
        mroomId = (TextView) view.findViewById(R.id.room_id);
        String roomdIdText =getArguments().getString("roomIdText");
        mroomId.setText(roomdIdText);
        //initViews(view);
        return view;
    }*/

    /*private void initViews(View view) {

        mroomId = (TextView) view.findViewById(R.id.room_id);
    }*/


}
