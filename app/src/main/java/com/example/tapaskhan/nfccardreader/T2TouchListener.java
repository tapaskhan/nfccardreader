package com.example.tapaskhan.nfccardreader;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class T2TouchListener implements View.OnTouchListener {

    private int _xDelta;
    private int _yDelta;
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        final int X=(int) motionEvent.getRawX();
        final int Y=(int)motionEvent.getRawY();
        switch(motionEvent.getAction() & MotionEvent.ACTION_MASK){
            case MotionEvent.ACTION_DOWN :
                RelativeLayout.LayoutParams relativeLayoutParams=(RelativeLayout.LayoutParams)view.getLayoutParams();
                _xDelta=X-relativeLayoutParams.leftMargin ;
                _yDelta=Y-relativeLayoutParams.topMargin;
                break;
            case MotionEvent.ACTION_UP :
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE :
                RelativeLayout.LayoutParams layoutParams=(RelativeLayout.LayoutParams)view.getLayoutParams();
                layoutParams.leftMargin=X-_xDelta;
                layoutParams.topMargin=Y-_yDelta;
                layoutParams.rightMargin=-250;
                layoutParams.bottomMargin=-250;
                view.setLayoutParams(layoutParams);

                break;
        }

        return true;
    }



}
